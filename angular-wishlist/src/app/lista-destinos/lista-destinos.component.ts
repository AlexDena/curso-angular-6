import { Component, Output, OnInit, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
})
export class ListaDestinosComponent implements OnInit {
   @Output() onItemAdded: EventEmitter<DestinoViaje>;

    constructor(private destinosApiClient:DestinoViaje){
     this.onItemAdded = new EventEmitter();
    }

  ngOnInit(){
  }

  agregado(d: DestinoViaje){
  //	this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(e:DestinoViaje){
  		  e.setSelected(true);
  }

}
