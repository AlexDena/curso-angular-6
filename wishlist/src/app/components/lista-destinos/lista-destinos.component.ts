import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { Store }  from '@ngrx/store'; 
import { AppState }  from '../../app.module'; 
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [ DestinosApiClient ]
})

export class ListaDestinosComponent implements OnInit {
@Output() onItemAdded: EventEmitter<DestinoViaje>;
updates: string[];
all;
// destinos:DestinoViaje[];
 
  constructor(private  destinosApiClient:DestinosApiClient, public store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates=[];
    store.select(state => state.destinos.items).subscribe(items => this.all =items);
  //	this.destinos =[];
 //   this.destinosApiClient.subscribeOnChange((d:DestinoViaje) => {
  //    if (d != null){
   //     this.updates.push('se a elegido a '+ d.nombre);
    //  }
    //});
   }

  ngOnInit() {
    this.store.select ( state => state.destinos.favorito)
    .subscribe(data =>{
        const f=data;
        if (f != null){
          this.updates.push('se a elegido a '+ f.Nombre);
        }
    });

  }
  
agregado(d:DestinoViaje){
   // this.destinos.push(d);
 //  console.log("llegueaqui");
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  //  this.store.dispatch(new NuevoDestinoAction(d));
}

  //guardar(nombre:string, url:string):boolean{
  //		this.destinos.push(new DestinoViaje(nombre,url));
  //		console.log(new DestinoViaje);
  //		console.log(this.destinos);	
  //		return false;
//  }

  elegido (e:DestinoViaje) {
   // this.destinos.forEach(function(x) {x.setSelected(false);});
   // this.destinosApiClient.getAll().forEach( x => x.setSelected(false));
   //segunda parte
   this.destinosApiClient.elegir(e);
    //this.store.dispatch(new ElegidoFavoritoAction(e));
   // e.setSelected(true);
   // return false;
  }

 // getAll(): DestinoViaje[] {
  //  return this.destinos;
 // }

}

