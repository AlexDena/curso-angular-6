import {Injectable, Inject, forwardRef} from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { Store } from '@ngrx/store';
import {
    NuevoDestinoAction,
    ElegidoFavoritoAction
  } from './destinos-viajes-state.model';
import { HttpRequest, HttpHeaders, HttpClient, HttpEvent, HttpResponse } from '@angular/common/http';
import {AppState, APP_CONFIG, AppConfig, MyDatabase, db} from './../app.module';
//import {Subject, BehaviorSubject} from 'rxjs';

@Injectable()
export class DestinosApiClient {
 destinos: DestinoViaje[] = [];
//current: Subject<DestinoViaje>= new BehaviorSubject<DestinoViaje>(null);  

constructor(
  private store: Store<AppState>,
  @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
  private http: HttpClient
) {
  this.store
    .select(state => state.destinos)
    .subscribe((data) => {
      console.log('destinos sub store');
      console.log(data);
      this.destinos = data.items;
    });
  this.store
    .subscribe((data) => {
      console.log('all store');
      console.log(data);
    });
}

add(d: DestinoViaje) {
   // this.destinos.push(d);
  // this.store.dispatch(new NuevoDestinoAction(d));
  console.log("agrege *****");
  const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
  const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.Nombre }, { headers: headers });
  this.http.request(req).subscribe((data: HttpResponse<{}>) => {
    if (data.status === 200) {
      this.store.dispatch(new NuevoDestinoAction(d));
      const myDb = db;
      myDb.destinos.add(d);
      console.log('todos los destinos de la db!');
      myDb.destinos.toArray().then(destinos => console.log(destinos))
    }
  });
}

elegir(d: DestinoViaje){
    this.store.dispatch(new ElegidoFavoritoAction(d));
    // this.destinos.forEach(x => x.setSelected(false));
     //d.setSelected(true);
     //this.current.next(d);
 }


//getAll(): DestinoViaje[] {
//    return this.destinos;
//}

getById(id: String): DestinoViaje {
  return this.destinos.filter(function(d) { return d.id.toString() === id; })[0];
}



// subscribeOnChange(fn){
  //      this.current.subscribe(fn);
 //}


}