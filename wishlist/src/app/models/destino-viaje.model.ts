import {v4 as uuid} from 'uuid';

export class DestinoViaje {

	 selected: boolean;
	 servicios: string[];
	 id = uuid();
	 public votes = 0;

	constructor(public Nombre:string, public Url:string){
		this.servicios =['pileta','desayuno'];

	}


	setSelected(s: boolean) {
		this.selected = s;
	  }

		isSelected():boolean{
			return this.selected;
		}

		voteUp(): any {
			this.votes++;
		  }
		  voteDown(): any {
			this.votes--;
		  }
		

}	